## Requirements

```bash
pip install bumpversion
```

Check the [.bumpversion.cfg](.bumpversion.cfg) to make sure:
* the `[bumpversion:file:src/__init__.py]` line reflects the file where you maintain the current version number (you can have multiple `[bumpversion:file:<somefile>]` lines)
* the `current_version` field matches your current version
* the x.y(-dev)? matches your semantic versioning choice

## Releasing a new version, e.g. v1.1-dev -> v1.1

```bash
bumpversion release [--verbose]
```

Will:
* Replace all occurrences of `1.1-dev` in your version files with `1.1`
* Will add the line `**v{new_version}**` beneath the line `**vNEXT**` in your [CHANGELOG.md](CHANGELOG.md)
* Will make a commit with the message `Bump version: 1.1-dev → 1.1`
* Tag the new commit with the tag `v1.1`

## Working on the next release, e.g. v1.1 -> v1.2-dev

```bash
bumpversion minor --no-tag [--verbose]
```

Will:
* Replace all occurrences of `1.1` in your version files with `1.2-dev`
* Will make a commit with the message `Bump version: 1.1-dev → 1.1`

Side effect:
* Will add the line `**v1.2-dev**` to the [CHANGELOG.md](CHANGELOG.md). That's usually not desirable and should be removed during development (and before making a new release)

**N.B.** `--no-tag` is added because development version bumps are not releases and should not be tagged

## Releasing a major version, e.g. v1.2-dev to v2.0

```bash
# v1.2-dev to v2.0-dev
bumpversion major --no-tag
# v2.0-dev to v2.1
bumpversion release
```

This requires 2 steps, one to bump the major version, one to take the `-dev` tag off.

This will also edit the CHANGELOG twice so be careful and never `bumpversion release` without checking your CHANGELOG.

# How to release

Using the following branches:
* master for stable code and releases (commits with tags)
* develop as the main development and staging branch

## Starting on `develop`:
* `git pull` to make sure all references are up to date

## On `master`, pushing `vX.Y`
* `git pull` to make sure all references are up to date
* `git merge develop` to merge staged changes
* `bumpversion release` to update the version and tag the commit
* `git push --tags` to push the commits and new tag to `master`

## Back on `develop`, pushing `vX.Z-dev`
* `git merge master`
* `bumpversion minor --no-tag` to bump to a development version
* Remove **vX.Z-dev** from the CHANGELOG (you can also do this later) and commit the change
* `git push` to push the changes to `develop`


Shortcut: you can use `git pull --all` to avoid having to do multiple pulls.

**Script (alpha)**

```bash
git pull --all
git checkout master
git merge develop
bumpversion release
git checkout develop
bumpversion minor --no-tag
git push --all && git push --tags
```
